#!/usr/bin/env python

import pandas as pd
import numpy as np
from keras.preprocessing import sequence, text
from keras.models import load_model

model = load_model("model.h5")
tk = text.Tokenizer()
question1 = raw_input('question1: ')
question2 = raw_input('question2: ')
data = [[question1, question2]]
df = pd.DataFrame(data, columns=['question1','question2'])

x1 = tk.texts_to_sequences(df.question1.values.astype(str))
x1 = sequence.pad_sequences(x1, maxlen=max_len)
x2 = tk.texts_to_sequences(df.question2.values.astype(str))
x2 = sequence.pad_sequences(x2, maxlen=max_len)

print x1
print x2
word_index = tk.word_index

pred = model.predict([x1,x2,x1,x2,x1,x2])

print pred
