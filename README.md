## Program Modules-
1. Data Pre-processing Module
2. GloVe Word Embeddings processing Module
3. Neural Network Module
4. Prediction Module


1. Data Pre-processing Module
> This module cleans the dataset.
> It replaces symbols to their respective word. (Like $ to dollar)
> It also removes punctuations.


2. GloVe Word Embeddings processing Module
> Used for extracting word embeddings for assigning weights to input layer.
> Serializes the extracted word embeddings for further use.


3. Neural Network Module
> For training the neural network.
> Reads the cleaned dataset, adds padding of lenght 40 to the questions.
> Trains the neural network and saves the model of best validation accuracy.

4. Prediction Module
> Predicts the similarity between the questions.
> Loads the module in memory.
> Takes input from user.
> Calculates the similarity.
> Print the similarity.

## Data that should be downlaoded before execution
> http://qim.fs.quoracdn.net/quora_duplicate_questions.tsv
> http://nlp.stanford.edu/data/glove.840B.300d.zip

## Execution Steps-
> 1. Run the pre_processing_data.py module.
> 2. Run the pickle_embb.py module.
> 3. Run the network.py module.
> 4. Run the predict.py module.
> 5. Give the input to predict.py as two questions.


