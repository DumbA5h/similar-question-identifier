import pandas as pd
import numpy as np
from tqdm import tqdm
import cPickle
from keras.preprocessing import sequence, text

data = pd.read_csv('clean_dataset.csv')
tk = text.Tokenizer(num_words=200000)

max_len = 40
tk.fit_on_texts(list(data.question1.values.astype(str)) + list(data.question2.values.astype(str)))
x1 = tk.texts_to_sequences(data.question1.values.astype(str))
x1 = sequence.pad_sequences(x1, maxlen=max_len)

x2 = tk.texts_to_sequences(data.question2.values.astype(str))
x2 = sequence.pad_sequences(x2, maxlen=max_len)

word_index = tk.word_index



embeddings_index = {}
f = open('/media/dumba5h/C89854029853EE06/Users/Ash/Downloads/glove.840B.300d/glove.840B.300d.txt')
for line in tqdm(f):
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

print('Found %s word vectors.' % len(embeddings_index))

embedding_matrix = np.zeros((len(word_index) + 1, 300))
for word, i in tqdm(word_index.items()):
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector



with open(r"embeddings.pickle", "wb") as output_file:
    cPickle.dump(embedding_matrix, output_file)

print 'dumped'
